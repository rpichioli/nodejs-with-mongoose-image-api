const express = require('express');
const mongoose = require('mongoose');
const validator = require('validator');
const fs = require('fs');

// Models
const Images = require('../models/images');

const router = express.Router();

/**
 * Get all images
 * @return List of images
 */
router.get('/', (req, res) => {
	// Fetch whole collection
	Images.find({}, (err, images) => {
		// Error returned
		if (err) res.status(400).json({ error: "Invalid request, something went wrong!" });
		// Invalid data received
		if (!images) res.status(401).json({ error: "Unauthorized action!" });

		// Sort images bringing latest first
		images = images.sort((a,b) => {
			if (a._id < b._id) return 1;
			if (a._id > b._id) return -1;
			return 0;
		});

		// Everything OK
		res.json({ success: true, images });
	});
});

/**
 * Get image content typed by it's original extension filtering by MongoDB _ID
 * @return Image itself with proper content-type and binary information
 */
router.get('/:id', (req, res) => {
	let _id = req.params.id || null;

	// Basic identifier validation
	if (!_id || validator.isEmpty(_id)) res.status(400).json({ success: false, error: "Invalid identifier has been sent!" });

	// Converting ID to OID through mongoose
	_id = mongoose.Types.ObjectId(_id);

	// Find the record
	Images.findById({ _id }, (err, doc) => {
		// Error returned
		if (err) res.status(400).json({ error: "Invalid request, something went wrong!" });
		// Everything OK
		res.contentType(doc.image.contentType);
		res.send(doc.image.data);
	});
});

/**
 * Insert image in database
 */
router.post('/', (req, res) => {
	// Debugging received request data
	// console.log('sent files', req.files);
	// console.log('post params', req.body);

	// Destructing received params
	const {path, type, originalFilename, name} = req.files.file;
	const {legend} = req.body;

	// MongoDB ID
	const _id = mongoose.Types.ObjectId();
	// Get image buffer to save into database
	const data = fs.readFileSync(path);
	// Image object to embed in collection
	const image = { data, contentType: type };

	// Basic validation
	if (!req.files || !req.body) res.status(400).json({ error: "Invalid request, provide all required data!" });

	// Insert into database
	Images.create({ _id, filename: name, legend, image}, (err, image) => {
		// Error returned
		if (err) res.status(400).json({ error: "Invalid request, something went wrong!", err });
		// Everything OK
		res.status(201).json({ success: true, image });
	});
})

/**
 * @description Delete image
 */
router.delete('/:id', (req, res) => {
	const _id = req.params.id || null;
	// Remove image by _ID
	if (_id) {
		// Remove record
		Images.deleteOne({ _id }, err => {
			// Something wrong happens
			if (err) res.status(400).json({ success: false, error: "Can't remove image!" });
			// Everything OK
			res.json({ success: true });
		});
	} else {
		res.status(400).json({ error: "Source required to perform the search!" });
	}
});

module.exports = router;
