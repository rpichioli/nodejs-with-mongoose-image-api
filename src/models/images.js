const mongoose = require('mongoose');

const imagesSchema = mongoose.Schema({
	_id: mongoose.Schema.Types.ObjectId,
	filename: String,
	legend: String,
	image: { data: Buffer, contentType: String }
}, {
	versionKey: false // You should be aware of the outcome after set to false
});

const Images = mongoose.model('Images', imagesSchema);

module.exports = Images;
