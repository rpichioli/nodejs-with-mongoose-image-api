const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const os = require("os");
const formData = require("express-form-data");
require('dotenv').config();

// Express application
const app = express();

// MongoDB (Mongoose) connection
const db = mongoose.connect(process.env.CONNECTION, { useNewUrlParser: true });

// Event interceptors - Reminder: mongoose.connection.readyState - to check status
mongoose.connection.on('connected', () => console.log('MongoDB (Mongoose) - Connected'));
mongoose.connection.on('error', error => console.log('MongoDB (Mongoose) - Error', error));
mongoose.connection.on('disconnected', () => console.log('MongoDB (Mongoose) - Disconnected'));

// Body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Module to parse multipart/form data
app.use(formData.parse())

// Middleware configuring whole routes for cross-origin requests
app.use(require('./middlewares/cors').allowAll);

// All images API under route gallery
app.use('/gallery/', require('./routes/gallery'));

// Default error handler - Detect any error in Express and handle it in middleware
app.use((err, req, res, next) => {
	// Runtime error output in server terminal
	console.error(err);
	// Send generic error message under 500 status response
	res.status(500).json({ error: 'Something went wrong! Verify the URL you tried to access and if you are filling the parameters correctly if they are needed.' });
});

// Not allowed access setup for every route that's not mapped
app.use((req, res) => res.status(403).json({ error: 'Forbidden access! URL not allowed.' }));

// Starting server
app.listen(process.env.PORT, () => console.log(`API server online - Listening: ${process.env.PORT}`));
