# nodejs-with-mongoose-image-api

## Preview
Express NodeJS server exposing RESTful API focusing in images management.


## Important topics
- Apply middleware module to allow CORS;
- Handling different kind of request sent information;
- File system management;
- OS identification;
- Converting file to binary and translating binary based in content-type;
- Responses based in validation and process status;
- And more..


## Requirements
Be sure you have **node** and **npm** installed, you can download easily in the [official website](https://nodejs.org/en/download/).

After the installation you can open the terminal and test both node and npm typing
```
node -v
```
This way you'll get node's respective version if the installation was fully OK.
```
npm -v
```
This way you test the npm, the same way you test node.

If you have both messages you are ready to go! :)

The **npm** is used to install and manage project dependencies and run it by command line, **becomes with node instalation**.


## How to use
With everything configured, open your terminal and go to the `src` folder under the root of the repository you cloned.

Run `npm i` to install all the dependencies. Wait a little bit - This process may take a while.

Run `npm start` to start the application and that's all.

To stop the running application press `ctrl + c` and `S` after.


## Developed by
Rodrigo Quiñones Pichioli - since July 3, 2019
